#!/usr/bin/perl -w
#
#    This file is part of Gink.
#
#    Gink is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#==============================================================================
#
# Ping.pm
#
# class 'Ping'
# This is a sub class of Nettool
#
# Copyright 2003 Yann Le Thieis
# Contact: Yann Le Thieis - <lethieis@yahoo.fr>
#
# Pod description after the code
#
#==============================================================================
# $Id: Ping.pm,v 1.1.1.1 2003/01/24 08:35:21 peredin Exp $
#==============================================================================
package Ping;

require 5.000;
use strict 'vars', 'refs', 'subs';

BEGIN
{
    use lib "./src";
    use Nettool;
    our @ISA = ("Nettool");
}

my $true = 1;
my $false = 0;


#==============================================================================
# Creation of a new ping tool
#==============================================================================
sub new_nettool
{
    my ($class,$path,$cmd,$fontname) = @_;
    my $ping = $class->new ($path, $cmd);
    $ping->{'count'} = 0;
    return $ping;
}


#==============================================================================
# Initializations
#==============================================================================
sub inits
{
    my ($ping,$names,$params,$count) = @_;
    $ping->{'names'} = $names;
    $ping->{'parameters'} = $params;
    $ping->{'count'} = $count;

    return $true;
}


#==============================================================================
# Actualizations of ping
#==============================================================================
sub actualize
{
    my ($ping,$inc) = @_;
    $ping->{'process_line'} += $inc;

    # Calculate process bar
    my $pbar = $ping->{'process_state'};
    if ($ping->{'count'} > 0)
    {
	$pbar = $ping->{'process_line'} / ($ping->{'count'} + 2);
	$pbar = 1 if ($pbar > 1);
    }
    else
    {
	($pbar == 0) && ($pbar = 1) || ($pbar = 0);
    }
    $ping->{'process_state'} = $pbar;

    return $true;
}

1;

__END__
