#!/usr/bin/perl -w
#
#    This file is part of Gink.
#
#    Gink is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#==============================================================================
#
# Gink.pm
#
# UI class 'Gink' (version 0.01)
# This is the 'Gink' class
#
# Copyright 2003 Yann Le Thieis
# Contact: Yann Le Thieis - <lethieis@yahoo.fr>
#
# Pod description after the code
#
#==============================================================================
# $Id: Gink.pm,v 1.3 2003/02/28 13:41:57 peredin Exp $
#==============================================================================
package app_gink;
require 5.000;
use strict 'vars', 'refs', 'subs';

BEGIN
{
    use lib "./src";
    use Nettool;
    use Finger;
    use Ping;
    use Tcpdump;
    use Subpropertybox_settings;
    use GinkUI;
    # We need the Gnome bindings as well
#    use Gnome;
}


#==============================================================================
# Permanent declarations
#==============================================================================

my $true = 1;
my $false = 0;

# gink config file state
my $changed_config_file = $false;

# Colors for the results
my $color_close;
my $color_open;
my $color_num;
my $color_search;
my $color_ip;
my $color_listen;
my $color_tcp;
my $color_udp;
my $color_raw;
my $color_bg;
my $color_base;

# Background font color for results
my $bg_color;

# Font for the result
my $font;

# GnomeAppbar declarations
my $appbar_timer = 0;
my $progress_bar;

# notebook pages
my $finger_page = 0;
my $trace_page = 1;
my $ping_page = 2;
my $portscan_page = 3;
my $tcpdump_page = 4;
my $netstat_page = 5;

# Nettool objects
my $finger = Finger->new ('finger');
my $traceroute = Nettool->new ('traceroute');
my $ping = Ping->new_nettool ('ping');
my $portscan = Nettool->new ('nmap');
my $tcpdump = Tcpdump->new_nettool ('tcpdump');
my $netstat = Nettool->new ('netstat');


#==============================================================================
# Main Gtk routine launch
#==============================================================================

sub app_run
{
    my ($class) = @_;
    $class->load_translations ('Gink');

    # You can use the line below to load a test .mo file before it is installed
    # in the normal place (eg /usr/local/share/locale/fr_FR@euro/LC_MESSAGES/
    # Gink.mo)
    #    $class->load_translations('Gink', 'test', undef,
    #    '/home/yann/Projets/gink/ppo/Gink.mo');

    Gnome->init ("$PACKAGE", "$VERSION");
    my $window = $class->new;
    $window->TOPLEVEL->show;

    set_actual_colors_from_gink_config ();

    # Get the config font
    my $font_name = Gnome::Config->get_string ('/gink/font/police');
    $font = Gtk::Gdk::Font->load ($font_name);

    # Finger UI initialisation
    my $opt = $finger->options;
    if ($opt =~ /-s/)
    {
	$window->FORM->{'checkbutton_finger_opt_s'}->set_active ($true);
    }
    if ($opt =~ /-l/)
    {
	$window->FORM->{'checkbutton_finger_opt_l'}->set_active ($true);
    }
    if ($opt =~ /-p/)
    {
	$window->FORM->{'checkbutton_finger_opt_p'}->set_active ($true);
    }
    if ($opt =~ /-m/)
    {
	$window->FORM->{'checkbutton_finger_opt_m'}->set_active ($true);
    }

    # Traceroute UI initialisation
    $opt = $traceroute->options;
    if ($opt =~ /-v/)
    {
	$window->FORM->{'checkbutton_traceroute_verbose'}->set_active ($true);
    }
    if ($opt =~ /-n/)
    {
	$window->FORM->{'checkbutton_traceroute_resolve'}->set_active ($true);
    }
    if ($opt =~ /-F/)
    {
	$window->FORM->{'checkbutton_traceroute_fragment'}->set_active ($true);
    }
    if ($opt =~ /-d/)
    {
	$window->FORM->{'checkbutton_traceroute_debug'}->set_active ($true);
    }
    if ($opt =~ /-m/)
    {
	$window->FORM->{'checkbutton_traceroute_hops'}->set_active ($true);
    }
    if ($opt =~ /-l/)
    {
	$window->FORM->{'checkbutton_traceroute_ttl'}->set_active ($true);
    }
    if ($opt =~ /-I/)
    {
	$window->FORM->{'radiobutton_traceroute_icmp'}->set_active ($true);
    }
    else
    {
	$window->FORM->{'radiobutton_traceroute_udp'}->set_active ($true);
    }
    if ($opt =~ /-t 16/)
    {
	$window->FORM->{'radiobutton_traceroute_low'}->set_active ($true);
    }
    elsif ($opt =~ /-t 8/)
    {
	$window->FORM->{'radiobutton_traceroute_high'}->set_active ($true);
    }
    else
    {
	$window->FORM->{'radiobutton_traceroute_none'}->set_active ($true);
    }

    # Extras UI initialisation (eg signal_connect) calls
    # Callbacks registration


    # Now let Gtk handle signals
    Gtk->main;
    $window->TOPLEVEL->destroy;

    return $window;
}


#==============================================================================
#=== Below are the default signal handlers for 'app_gink' class
#==============================================================================
sub about_Form
{
    my ($class) = @_;
    my $gtkversion = Gtk->major_version.".".Gtk->minor_version.".".Gtk->micro_version;
    my $name = $0;
    #
    # Create a Gnome::About '$ab'
    my $ab = new Gnome::About ($PACKAGE,
			       $VERSION,
			       _("Copyright")." $DATE",
			       $AUTHOR,
			       _("Net Tools Box")."\n\n".
			       "Gtk "._("version").": $gtkversion\n".
			       "Gtk-Perl "._("version").
			       ": $Gtk::VERSION\n". ` gnome -
				   config-- version `."\n\n".
			       _("run from file").": $name\n".
			       "# Unspecified copying policy, please contact the author\n# ",
			       "$Glade::PerlRun::pixmaps_directory/glade2perl_logo.xpm",);
    $ab->set_title (_("About")." Gink");
    $ab->position ('mouse');
    $ab->set_policy (1, 1, 0);
    $ab->set_modal (1);
    $ab->show;
}
# End of sub about_Form

sub destroy_Form
{
    # if exists stop the repeating call to sub timeout_show_appbar
    Gtk->timeout_remove( $appbar_timer ) if ($appbar_timer);

    Gtk->main_quit;
}

sub toplevel_hide
{
    shift->get_toplevel->hide
}

sub toplevel_close
{
    shift->get_toplevel->close
}

sub toplevel_destroy
{
    shift->get_toplevel->destroy
}


#==============================================================================
# Below are the signal handlers for 'app_gink' class
#==============================================================================
sub gtk_main_quit
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->gtk_main_quit";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    # Show the asking confirmation window

    # if exists stop the repeating call to sub timeout_show_appbar
    Gtk->timeout_remove( $appbar_timer ) if ($appbar_timer);

    Gtk->main_quit;
}

sub on_about_gink_activate
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_about1_activate";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    about_Form ($class);
}


#==============================================================================
# Activation of the gink settings
#==============================================================================

sub on_preferences_gink_activate
{
    Subpropertybox_settings->app_run(
				     \$changed_config_file,
				     $finger,
				     $traceroute,
#				     $ping,
#				     $portscan,
#				     $tcpdump,
#				     $netstat,
				    );
    return $true;
}


#==============================================================================
# Checking for a modified config file on each action on each page
#==============================================================================
sub on_app_gink_set_focus_child
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_app_gink_set_focus_child";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};


    # If the config file changed then get the new config
    # to set font and result colors.
    if ($changed_config_file)
    {
	my $font_name = Gnome::Config->get_string ('/gink/font/police');
	$font = Gtk::Gdk::Font->load ($font_name);
	set_actual_colors_from_gink_config ();
	$changed_config_file = $false;
    }
    return $true;
}
# End of sub on_app_gink_set_focus_child


#==============================================================================
# finger functions
#==============================================================================

my $finger_timer;

# Action attached on the clicked finger query button
# --------------------------------------------------
sub on_button_finger_query_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_button_finger_query_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    # Get the finger number page in the notebook
    $finger_page = $form->{'notebook_gink'}->get_current_page();

    if (not $finger->running)
    {
	# Get infos and make inits
	$form->{'text_finger_result'}->delete_text ();
	my $name = $form->{'entry_finger_name'}->get_text ();
	my $host = $form->{'entry_finger_athost'}->get_text ();
	$finger->inits($name, $host);

	# Start the background finger process
	$finger->start;

	# Show the command with the options
	$form->{'appbar_gink_status'}->push ($finger->path.$finger->cmd.$finger->options." ".$finger->param);

	# Prepare the cancel action for the next click on the button
	$form->{'button_finger_query'}->child->set("  Cancel  ");

	# launch "appbar reinit auto" if doesn't done!
	$appbar_timer = Gtk->timeout_add( 100, \&timeout_show_appbar, $form ) if not($appbar_timer);

	# Launch periodically called function
	$finger_timer = Gtk->timeout_add( 100, \&timeout_finger_processing, $form );
    }
    else
    {
	# End the background finger processes
	Gtk->timeout_remove( $finger_timer );
	$finger->stop("all");

	# Finger is ready to be launched
	$form->{'button_finger_query'}->child->set("   Finger   ");
    }

    return($true);
}
# End of sub on_button_finger_query_clicked


# Get the result of the backgroung finger process in the temp finger file
# -----------------------------------------------------------------------
sub timeout_finger_processing
{
    my $form = shift;
    my @record = $finger->get_background_process_result;

    # For each lines insert them in the Gtk text widget.
    foreach (@record)
    {
	my $finger_ent = $_;

	# At the end of the temp file (and finger background process)
	# make finger to be ready to launch again.
	if ($finger_ent =~ /EOF/)
	{
	    # Open a message box error if some errors occured 
	    # during background processing
	    my $error = "";
	    foreach ($finger->get_errors)
	    {
		$error .= "\n".$_ ;
	    }
	    message_box_error($error) if ($error);

	    # End of the finger process
	    $finger->stop;

	    # Make finger ready to be launched
	    Gtk->timeout_remove( $finger_timer );
	    $form->{'button_finger_query'}->child->set("   Finger   ");
	}
	else
	{
	    # Actualize finger parameters
	    $finger->actualize (1);

	    # Color processing
	    insert_colored_result (
				   $form,
				   'text_finger_result',
				   $font,
				   undef,
				   $finger_ent,
				   $finger->names,
				  );
	}
    }

    # show the progress if the actual page is the finger page in
    # the notebook
    if ($form->{'notebook_gink'}->get_current_page() == $finger_page)
    {
	$progress_bar = $finger->process_state;
    }

    return($true);
}
# End of sub timeout_finger_processing


# Function to show or not the options frame
# -----------------------------------------
sub on_togglebutton_finger_opts_toggled
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_togglebutton_finger_opts_toggled";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    # If options button is pushed then show the options frame
    if ($form->{'togglebutton_finger_opts'}->active)
    {
	$form->{'frame_finger_options'}->show;
    }
    else
    {
	$form->{'frame_finger_options'}->hide;
    }
    return $true;
}


# Function to update finger object (and UI) when one option is checked
# --------------------------------------------------------------------
sub on_checkbutton_finger_opt_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_checkbutton_finger_opt_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    my $opt = "";
    $opt .= " -s" if ( $form->{'checkbutton_finger_opt_s'}->active );
    $opt .= " -l" if ( $form->{'checkbutton_finger_opt_l'}->active );
    $opt .= " -p" if ( $form->{'checkbutton_finger_opt_p'}->active );
    $opt .= " -m" if ( $form->{'checkbutton_finger_opt_m'}->active );
    $finger->set_options ($opt);

    # Show the command with the options
    $form->{'appbar_gink_status'}->push ($finger->path.$finger->cmd.$opt." ".$finger->param);

    return $true;
}
# End of sub on_checkbutton_finger_opt_clicked


# Updating finger with gink config file
# -------------------------------------
sub on_button_finger_get_config_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_button_finger_get_config_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    # Get the defaults in the config file
    my $opt = Gnome::Config->get_string ('/gink/finger/options');

    # Applying these to the UI
    if ($opt =~ /-s/)
    {
	$form->{'checkbutton_finger_opt_s'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_finger_opt_s'}->set_active ($false);
    }
    if ($opt =~ /-l/)
    {
	$form->{'checkbutton_finger_opt_l'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_finger_opt_l'}->set_active ($false);
    }
    if ($opt =~ /-p/)
    {
	$form->{'checkbutton_finger_opt_p'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_finger_opt_p'}->set_active ($false);
    }
    if ($opt =~ /-m/)
    {
	$form->{'checkbutton_finger_opt_m'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_finger_opt_m'}->set_active ($false);
    }
    $finger->set_options ($opt);

    return $true;
}
# End of sub on_button_finger_get_config_clicked



#==============================================================================
# traceroute functions
#==============================================================================

my $trace_timer;
my $trace_color = $color_num;

# Action attached on the clicked traceroute query button
# ------------------------------------------------------
sub on_button_traceroute_query_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_button_traceroute_query_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    # Get the traceroute number page in the notebook
    $trace_page = $form->{'notebook_gink'}->get_current_page();

    if ( not $traceroute->running )
    {
        # Get infos and make inits
	my $address = $form->{'entry_traceroute_host'}->get_text ();
	$form->{'text_traceroute_result'}->delete_text ();
	if (not $address)
	{
	    message_box_error ("No address for traceroute");
	    return($false);
	}
	$traceroute->inits($address,$address);

	# Start the background traceroute process
	$traceroute->start;

	# Show the command with the options
	$form->{'appbar_gink_status'}->push ($traceroute->path.$traceroute->cmd.$traceroute->options." ".$traceroute->param);

	# Prepare the cancel action for the next click on the button
	$form->{'button_traceroute_query'}->child->set("  Cancel  ");

	# Launch "appbar reinit auto" if doesn't done!
	$appbar_timer = Gtk->timeout_add( 100, \&timeout_show_appbar, $form ) if not($appbar_timer);

	# Launch periodically called function	
	$trace_timer = Gtk->timeout_add( 100, \&timeout_traceroute_processing, $form );
    }
    else
    {
	# End the background traceroute processes
	Gtk->timeout_remove( $trace_timer );
	$traceroute->stop("all");

	# Traceroute is ready to be launched
	$form->{'button_traceroute_query'}->child->set("   Trace   ");
    }
    return ($true);
}
# End of sub on_button_traceroute_query_clicked


# Get the result of the backgroung traceroute process in the temp finger file
# ---------------------------------------------------------------------------
sub timeout_traceroute_processing
{
    my $form = shift;

    # Get the last lines from the traceroute background process
    my @record = $traceroute->get_background_process_result;

    # For each last lines in the temp file insert them in the Gtk text widget.
    foreach (@record)
    {
	my $trace_ent = $_;

	# At the end of the temp file (and traceroute background process)
	# make traceroute to be ready to launch again.
	if ($trace_ent =~ /EOF/)
	{
	    # Open a message box error if some errors occured 
	    # during background processing
	    my $error = "";
	    foreach ($traceroute->get_errors)
	    {
		$error .= "\n".$_ ;
	    }
	    message_box_error($error) if ($error);

	    # End the traceroute process
	    $traceroute->stop;

	    # Make traceroute ready to be launched
	    Gtk->timeout_remove( $trace_timer );
	    $form->{'button_traceroute_query'}->child->set("   Trace   ");
	}
	else
	{
	    # Actualize traceroute parameters
	    $traceroute->actualize(1);

	    # Color processing
	    (not $bg_color) && ($bg_color = $color_bg) || ($bg_color = undef);
	    insert_colored_result (
				   $form,
				   'text_traceroute_result',
				   $font,
				   $bg_color,
				   $trace_ent,
				   $traceroute->names,
				  );
	}
    }

    # show the progress if the actual page is the traceroute page in
    # the notebook
    if ($form->{'notebook_gink'}->get_current_page() == $trace_page)
    {
	$progress_bar = $traceroute->process_state;
    }

    return($true);
}
# End of sub timeout_traceroute_processing

# Function to show or not the options frame
# -----------------------------------------
sub on_togglebutton_traceroute_opts_toggled
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_togglebutton_traceroute_opts_toggled";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    # If options button is pushed then show the options frame
    if ($form->{'togglebutton_traceroute_opts'}->active)
    {
	$form->{'frame_traceroute_opts'}->show;
    }
    else
    {
	$form->{'frame_traceroute_opts'}->hide;
    }
    return $true;
}

# Function to update traceroute object (and UI) when one option is checked
# ------------------------------------------------------------------------
sub on_traceroute_options_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_checkbutton_finger_opt_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    my $opt = "";
    $opt .= " -v" if ( $form->{'checkbutton_traceroute_verbose'}->active );
    if ( $form->{'checkbutton_traceroute_hops'}->active )
    {
	$opt .= " -t ".$form->{'entry_traceroute_hops'}->get_text ();
    }
    $opt .= " -n" if ( $form->{'checkbutton_traceroute_resolve'}->active );
    $opt .= " -F" if ( $form->{'checkbutton_traceroute_fragment'}->active );
    $opt .= " -d" if ( $form->{'checkbutton_traceroute_debug'}->active );
    $opt .= " -l" if ( $form->{'checkbutton_traceroute_ttl'}->active );
    if ( $form->{'radiobutton_traceroute_icmp'}->active )
    {
	$opt .= " -I";
    }
    else
    {
	my $p_value = $form->{'entry_traceroute_port'}->get_text ();
	if ($p_value != 33434)
	{
	    $opt .= " -p ".$p_value;
	}
    }
    $opt .= " -t 16" if ( $form->{'radiobutton_traceroute_low'}->active );
    $opt .= " -t 8" if ( $form->{'radiobutton_traceroute_high'}->active );
    $traceroute->set_options ($opt);

    # Show the command with the options
    $form->{'appbar_gink_status'}->push ($traceroute->path.$traceroute->cmd.$opt." ".$traceroute->param);

    return $true;
}
# End of sub on_traceroute_options_clicked

# Updating traceroute with gink config file
# -----------------------------------------
sub on_button_traceroute_get_config_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_button_traceroute_get_config_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    # Get the defaults in the config file
    my $opt = Gnome::Config->get_string ('/gink/traceroute/options');

    # Applying these to the UI
    if ($opt =~ /-v/)
    {
	$form->{'checkbutton_traceroute_verbose'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_traceroute_verbose'}->set_active ($false);
    }
    if ($opt =~ /-n/)
    {
	$form->{'checkbutton_traceroute_resolve'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_traceroute_resolve'}->set_active ($false);
    }
    if ($opt =~ /-F/)
    {
	$form->{'checkbutton_traceroute_fragment'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_traceroute_fragment'}->set_active ($false);
    }
    if ($opt =~ /-d/)
    {
	$form->{'checkbutton_traceroute_debug'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_traceroute_debug'}->set_active ($false);
    }
    if ($opt =~ /-m/)
    {
	$form->{'checkbutton_traceroute_hops'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_traceroute_hops'}->set_active ($false);
    }
    if ($opt =~ /-l/)
    {
	$form->{'checkbutton_traceroute_ttl'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_traceroute_ttl'}->set_active ($false);
    }
    if ($opt =~ /-I/)
    {
	$form->{'radiobutton_traceroute_icmp'}->set_active ($true);
    }
    else
    {
	$form->{'radiobutton_traceroute_udp'}->set_active ($true);
    }
    if ($opt =~ /-t 16/)
    {
	$form->{'radiobutton_traceroute_low'}->set_active ($true);
    }
    elsif ($opt =~ /-t 8/)
    {
	$form->{'radiobutton_traceroute_high'}->set_active ($true);
    }
    else
    {
	$form->{'radiobutton_traceroute_none'}->set_active ($true);
    }
    $finger->set_options ($opt);

    return $true;
}
# End of sub on_button_traceroute_get_config_clicked


#==============================================================================
# ping functions
#==============================================================================

my $ping_timer;
my $ping_color = $color_num;

# Action attached on the clicked ping query button
# ------------------------------------------------
sub on_button_ping_query_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_button_ping_query_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    my $ping_count = 0;

    # get the ping number page in the notebook
    $ping_page = $form->{'notebook_gink'}->get_current_page();

    if ( not $ping->running )
    {
        # Get infos and make inits
	my $address = $form->{'entry_ping_address'}->get_text ();
	my $params = $address;
	if (not $address)
	{
	    message_box_error ("No address for ping");
	    return($false);
	}
	if ($form->{'radiobutton_ping_choice2'}->active)
	{
	    $ping_count = $form->{'entry_ping_number'}->get_text ();
	    if ($ping_count < 1)
	    {
		message_box_error ("Count number not valid.");
		return ($false);
	    }
	    $params = "-c $ping_count ".$address;
	}
	$form->{'text_ping_result'}->delete_text ();
	$ping->inits($address, $params, $ping_count);

	# Start the ping background process
	$ping->start;

	# Prepare the cancel action for the next click on the button
	$form->{'button_ping_query'}->child->set("  Cancel  ");

	# Launch "appbar reinit auto" if doesn't done!
	$appbar_timer = Gtk->timeout_add( 100, \&timeout_show_appbar, $form ) if not($appbar_timer);

	# Launch periodically called function		
	$ping_timer = Gtk->timeout_add( 100, \&timeout_ping_processing, $form );
    }
    else
    {
	# End the background ping processes
	Gtk->timeout_remove( $ping_timer );
	$ping->stop("all");

	# Ping is ready to be launched
	$form->{'button_ping_query'}->child->set("   Ping   ");
    }
}
# End of sub on_button_ping_query_clicked


# Get the result of the backgroung ping process in the temp ping file
# -------------------------------------------------------------------
sub timeout_ping_processing
{
    my $form = shift;
    my @record = $ping->get_background_process_result;

    # For each last lines in the temp file insert them in the Gtk text widget.
    foreach (@record)
    {
	my $ping_ent = $_;

	# At the end of the temp file (and ping background process)
	# make ping to be ready to launch again.
	if ($ping_ent =~ /EOF/)
	{
	    # Open a message box error if some errors occured during background processing
	    my $error = "";
	    foreach ($ping->get_errors)
	    {
		$error .= "\n".$_ ;
	    }
	    message_box_error($error) if ($error);

	    # End the ping process
	    $ping->stop;

	    # Make ping ready to be launched
	    Gtk->timeout_remove( $ping_timer );
	    $form->{'button_ping_query'}->child->set("   Ping   ");
	}
	else
	{
	    $ping->actualize (1);

	    # Color processing
	    (not $bg_color) && ($bg_color = $color_bg) || ($bg_color = undef);
	    insert_colored_result (
				   $form,
				   'text_ping_result',
				   $font,
				   $bg_color,
				   $ping_ent,
				   $ping->names,
				  );
	}
    }

    # show the progress if the actual page is the ping page in
    # the notebook
    if ($form->{'notebook_gink'}->get_current_page() == $ping_page)
    {
	$progress_bar = $ping->process_state;
    }

    return($true);
}
# End of sub timeout_ping_processing


#==============================================================================
# Port scan functions
#==============================================================================

my $portscan_timer;
my $portscan_color = $color_num;

# Action attached on the clicked portscan query button
# ----------------------------------------------------
sub on_button_portscan_query_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_button_portscan_scan_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    # get the portscan number page in the notebook
    $portscan_page = $form->{'notebook_gink'}->get_current_page();

    if ( not $portscan->running )
    {
        # Get infos and make inits
	$form->{'text_portscan_result'}->delete_text ();
	my $address = $form->{'entry_portscan_address'}->get_text ();
	my $params = $address;
	if (not $address)
	{
	    message_box_error ("No address to scan");
	    return($false);
	}
	if ($form->{'checkbutton_portscan_range'}->active)
	{
	    my $borne1 = $form->{'entry_portscan_borne1'}->get_text();
	    my $borne2 = $form->{'entry_portscan_borne2'}->get_text();
	    if (($borne1 > 0) && ($borne2 > $borne1))
	    {
		$params = "-p $borne1-$borne2 ".$address;
	    }
	    else
	    {
		message_box_error ("These ports range is not valid");
		return($false);
	    }
	}
	$portscan->inits ($address, $params);

	# Start the portscan background process
	$portscan->start;

	# Prepare the cancel action for the next click on the button
	$form->{'button_portscan_query'}->child->set("  Cancel  ");

	# launch "appbar reinit auto" if doesn't done!
	$appbar_timer = Gtk->timeout_add( 100, \&timeout_show_appbar, $form ) if not($appbar_timer);

	# Launch periodically called function	
	$portscan_timer = Gtk->timeout_add( 100, \&timeout_portscan_processing, $form );
    }
    else
    {

	# End the background port scan processes
	Gtk->timeout_remove( $portscan_timer );
	$portscan->stop("all");

	# Portscan is ready to be launched
	$form->{'button_portscan_query'}->child->set("   Scan   ");
    }
}
# End of sub on_button_portscan_query_clicked


# Get the result of the backgroung port scan process in the temp port scan file
# -----------------------------------------------------------------------------
sub timeout_portscan_processing
{
    my $form = shift;
    my @record = $portscan->get_background_process_result;

    # For each last lines in the temp file insert them in the Gtk text widget.
    foreach (@record)
    {
	my $portscan_ent = $_;

	# At the end of the temp file (and portscan background process)
	# make portscan to be ready to launch again.
	if ($portscan_ent =~ /EOF/)
	{
	    # Open a message box error if some errors occured 
	    # during background processing
	    my $error = "";
	    foreach ($portscan->get_errors)
	    {
		$error .= "\n".$_ ;
	    }
	    message_box_error($error) if ($error);

	    # End the portscan process
	    $portscan->stop;

	    # Make portscan ready to be launched
	    Gtk->timeout_remove( $portscan_timer );
	    $form->{'button_portscan_query'}->child->set("   Scan   ");
	}
	else
	{
	    $portscan->actualize (1);

	    # Color processing
	    insert_colored_result (
				   $form,
				   'text_portscan_result',
				   $font,
				   undef,
				   $portscan_ent,
				   $portscan->names,
				  );
	}
    }

    # show the progress if the actual page is the portscan page in
    # the notebook
    if ($form->{'notebook_gink'}->get_current_page() == $portscan_page)
    {
	$progress_bar = $portscan->process_state;
    }

    return($true);
}
# End of sub timeout_portscan_processing


#==============================================================================
# Tcpdump functions
#==============================================================================

my $tcpdump_timer;
my $tcpdump_color = $color_num;


# Action attached on the clicked tcpdump query button
# ---------------------------------------------------
sub on_button_tcpdump_query_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_button_tcpdump_query_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    # get the tcpdump number page in the notebook
    $tcpdump_page = $form->{'notebook_gink'}->get_current_page();

    if ( not $tcpdump->running )
    {
        # Get infos from count choice
	my $params = "";
	my $tcpdump_count = 0;
	my $tcpdump_count_type = "";
	if ($form->{'radiobutton_tcpdump_packets'}->active)
	{
	    $tcpdump_count = $form->{'spinbutton_tcpdump_packets'}->get_text ();
	    $tcpdump_count_type = "packets";
	    $params .= " -c $tcpdump_count";
	}
	elsif ($form->{'radiobutton_tcpdump_kb'}->active)
	{
	    $tcpdump_count = $form->{'spinbutton_tcpdump_kb'}->get_text ();
	    $tcpdump_count_type = "kb";
	}
	elsif ($form->{'radiobutton_tcpdump_seconds'}->active)
	{
	    $tcpdump_count = $form->{'spinbutton_tcpdump_seconds'}->get_text ();
	    $tcpdump_count_type = "seconds";
	}
	if ($tcpdump_count < 1)
	{
	    message_box_error ("Count number in $tcpdump_count_type not valid.");
	    return ($false);
	}

	# Get infos from file choice
	if ( $form->{'radiobutton_tcpdump_file_capture'}->active
	     or $form->{'radiobutton_tcpdump_file_analyse'}->active )
	{
	    my $opt = $form->{'combo-entry_tcpdump_file'}->get_text ();
	    if (not $opt)
	    {
		message_box_error("A name file is needed");
		return $false;
	    }
	    if ($form->{'radiobutton_tcpdump_file_capture'}->active)
	    {
		$params .= " -w ".$opt;
	    }
	    else
	    {
		$params .= " -r ".$opt;
	    }
	}

	# Make inits
	$form->{'text_tcpdump_result'}->delete_text ();
	$tcpdump->inits ($params, $tcpdump_count, $tcpdump_count_type);

	# Start the tcpdump process
	$tcpdump->start;

	# Prepare the cancel action for the next click on the button
	$form->{'button_tcpdump_query'}->child->set("  Cancel  ");

	# Launch "appbar reinit auto" if doesn't done!
	$appbar_timer = Gtk->timeout_add( 100, \&timeout_show_appbar, $form ) if not($appbar_timer);

	# Launch periodically called function		
	$tcpdump_timer = Gtk->timeout_add( 100, \&timeout_tcpdump_processing, $form );
    }
    else
    {
	# End the background tcpdump processes
	Gtk->timeout_remove( $tcpdump_timer );
	$tcpdump->stop("all");

	# Tcpdump is ready to be launched
	$form->{'button_tcpdump_query'}->child->set("   Tcpdump   ");
    }
}
# End of sub on_button_tcpdump_query_clicked


# Get the result of the backgroung tcpdump process in the temp tcpdump file
# -------------------------------------------------------------------
sub timeout_tcpdump_processing
{
    my $form = shift;

    # Get the last lines from the temp tcpdump file
    my @record = $tcpdump->get_background_process_result;

    # For each last lines in the temp file insert them in the Gtk text widget.
    foreach (@record)
    {
	my $tcpdump_ent = $_;

	# At the end of the temp file (and tcpdump background process)
	# make tcpdump to be ready to launch again.
	if ($tcpdump_ent =~ /EOF/)
	{
	    # Open a message box error if some errors occured during background processing
	    my $error = "";
	    foreach ( $tcpdump->get_errors )
	    {
		$error .= "\n".$_ ;
	    }
	    message_box_error($error) if ($error);

	    # End the tcpdump process
	    $tcpdump->stop;

	    # Make tcpdump ready to be launched
	    Gtk->timeout_remove( $tcpdump_timer );
	    $form->{'button_tcpdump_query'}->child->set("   Tcpdump   ");
	}
	else
	{
	    if ( not $tcpdump->actualize (1) )
	    {
		# End the tcpdump process
		$tcpdump->stop("all");

		# Make tcpdump ready to be launched
		Gtk->timeout_remove( $tcpdump_timer );
		$form->{'button_tcpdump_query'}->child->set("   Tcpdump   ");
	    }

	    # Color processing
	    (not $bg_color) && ($bg_color = $color_bg) || ($bg_color = undef);
	    insert_colored_result (
				   $form,
				   'text_tcpdump_result',
				   $font,
				   $bg_color,
				   $tcpdump_ent,
				   $tcpdump->names,
				  );
	}
    }

    # show the progress if the actual page is the tcpdump page in
    # the notebook
    if ($form->{'notebook_gink'}->get_current_page() == $tcpdump_page)
    {
	$progress_bar = $tcpdump->process_state;
    }

    return($true);
}
# End of sub timeout_tcpdump_processing


#==============================================================================
# Netstat functions
#==============================================================================

my $netstat_timer;
my $netstat_color = $color_num;

# Action attached on the clicked netstat query button
# ---------------------------------------------------
sub on_button_netstat_query_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_button_portscan_scan_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    # get the netstat number page in the notebook
    $netstat_page = $form->{'notebook_gink'}->get_current_page();

    if ( not $netstat->running )
    {
        # Initializations
	$form->{'text_netstat_result'}->delete_text ();
	my $params = "";
	$netstat->inits (`hostname`, $params);

	# Start the netstat background process
	$netstat->start;

	# Prepare the cancel action for the next click on the button
	$form->{'button_netstat_query'}->child->set("  Cancel  ");

	# launch "appbar reinit auto" if doesn't done!
	$appbar_timer = Gtk->timeout_add( 100, \&timeout_show_appbar, $form ) if not($appbar_timer);

	# Launch periodically called function	
	$netstat_timer = Gtk->timeout_add( 100, \&timeout_netstat_processing, $form );
    }
    else
    {
	# End the background netstat processes
	Gtk->timeout_remove( $netstat_timer );
	$netstat->stop("all");

	# Netstat is ready to be launched
	$form->{'button_netstat_query'}->child->set("   Netstat   ");
    }
}
# End of sub on_button_netstat_query_clicked


# Get the result of the backgroung netstat process in the temp port scan file
# ---------------------------------------------------------------------------
sub timeout_netstat_processing
{
    my $form = shift;
    my @record = $netstat->get_background_process_result;

    # For each last lines in the temp file insert them in the Gtk text widget.
    foreach (@record)
    {
	my $netstat_ent = $_;

	# At the end of the temp file (and netstat background process)
	# make netstat to be ready to launch again.
	if ($netstat_ent =~ /EOF/)
	{
	    # Open a message box warning if some warnings occured 
	    # during background processing
	    my $warning = "";
	    foreach ( $netstat->get_warnings )
	    {
		$warning .= "\n".$_ ;
	    }
	    message_box_warning($warning) if ($warning);

	    # End the netstat process
	    $netstat->stop;

	    # Make netstat ready to be launched
	    Gtk->timeout_remove( $netstat_timer );
	    $form->{'button_netstat_query'}->child->set("   Netstat   ");
	}
	else
	{
	    $netstat->actualize (1);

	    # Color processing
	    (not $bg_color) && ($bg_color = $color_bg) || ($bg_color = undef);
	    insert_colored_result (
				   $form,
				   'text_netstat_result',
				   $font,
				   $bg_color,
				   $netstat_ent,
				   $netstat->names,
				  );
	}
    }

    # show the progress if the actual page is the netstat page in
    # the notebook
    if ($form->{'notebook_gink'}->get_current_page() == $portscan_page)
    {
	$progress_bar = $netstat->process_state;
    }

    return($true);
}
# End of sub timeout_netstat_processing


#==============================================================================
# Common functions
#==============================================================================


# Open a gnome message box error for gink with the specified label error
# ----------------------------------------------------------------------
sub message_box_error
{
    my $label = shift;
    my $mbox_error = new Gnome::MessageBox ($label, 'error','Button_Ok')
	|| die "Can't create a message box error!\n";
    $mbox_error->close_hides(0);
    $mbox_error->set_close(1);
    $mbox_error->position('mouse');
    $mbox_error->set_policy(0, 1, 0);
    $mbox_error->set_modal(1);
    $mbox_error->show;

    return $true;
}


# Open a gnome message box warning for gink with the specified label warning
# --------------------------------------------------------------------------
sub message_box_warning
{
    my $label = shift;
    my $mbox_warning = new Gnome::MessageBox ($label,'warning','Button_Ok')
	|| die "Can't create a message box warning!\n";
    $mbox_warning->close_hides(0);
    $mbox_warning->set_close(1);
    $mbox_warning->position('mouse');
    $mbox_warning->set_policy(0, 1, 0);
    $mbox_warning->set_modal(1);
    $mbox_warning->show;

    return $true;
}

#
# Actualization of the gtk progress bar
# -------------------------------------
sub timeout_show_appbar
{
    my $form = shift;

    # Get the actual function page and actualize the progress bar

 SWITCH: for ($form->{'notebook_gink'}->get_current_page())
    {
	
	/$finger_page/ && do
	{
	    $progress_bar = $finger->process_state;
	    # Show the command with the options
	    $form->{'appbar_gink_status'}->push ($finger->path.$finger->cmd.$finger->options." ".$finger->param);
	    last;
	};

	/$trace_page/ && do
	{
	    $progress_bar = $traceroute->process_state;
	    # Show the command with the options
	    $form->{'appbar_gink_status'}->push ($traceroute->path.$traceroute->cmd.$traceroute->options." ".$traceroute->param);
	    last;
	};

	/$ping_page/ && do
	{
	    $progress_bar = $ping->process_state;
	    # Show the command with the options
	    $form->{'appbar_gink_status'}->push ($ping->path.$ping->cmd.$ping->options." ".$ping->param);
	    last;
	};

	/$portscan_page/ && do
	{
	    $progress_bar = $portscan->process_state;
	    # Show the command with the options
	    $form->{'appbar_gink_status'}->push ($portscan->path.$portscan->cmd.$portscan->options." ".$portscan->param);
	    last;
	};

	/$tcpdump_page/ && do
	{
	    $progress_bar = $tcpdump->process_state;
	    # Show the command with the options
	    $form->{'appbar_gink_status'}->push ($tcpdump->path.$tcpdump->cmd.$tcpdump->options." ".$tcpdump->param);
	    last;
	};

	/$netstat_page/ && do
	{
	    $progress_bar = $netstat->process_state;
	    # Show the command with the options
	    $form->{'appbar_gink_status'}->push ($netstat->path.$netstat->cmd.$netstat->options." ".$netstat->param);
	    last;
	};
    }

    # Set the progress bar
    $form->{'appbar_gink_status'}->set_progress($progress_bar);

    return($true);
}
# End of sub timeout_show_appbar


# Function to process color and insert result in the Gtk text result area
# -----------------------------------------------------------------------
sub insert_colored_result
{
    my ($form,$text_result,$font,$bg_col,$ent,$name) = @_;

 SWITCH: for ($ent)
    {
	/\d+\.\d+\.\d+\.\d+/ && do
	{
	    insert_colored_result ($form,$text_result,$font,$bg_col,$`,$name);
	    $form->{$text_result}->insert($font,$color_ip,$bg_col,$&);
	    insert_colored_result ($form,$text_result,$font,$bg_col,$',$name);
	    last;
	};

	/\d+/ && do
	{
	    insert_colored_result ($form,$text_result,$font,$bg_col,$`,$name);
	    $form->{$text_result}->insert($font,$color_num,$bg_col,$&);
	    insert_colored_result ($form,$text_result,$font,$bg_col,$',$name);
	    last;
	};
	
	/$name/i && do
	{
	    insert_colored_result ($form,$text_result,$font,$bg_col,$`,$name);
	    $form->{$text_result}->insert($font,$color_search,$bg_col,$&);
	    insert_colored_result ($form,$text_result,$font,$bg_col,$',$name);
	    last;
	};

	/\w*\.\w+\.\w+/ && do
	{
	    insert_colored_result ($form,$text_result,$font,$bg_col,$`,$name);
	    $form->{$text_result}->insert($font,$color_ip,$bg_col,$&);
	    insert_colored_result ($form,$text_result,$font,$bg_col,$',$name);
	    last;
	};

	/listen\w*/i && do
	{
	    insert_colored_result ($form,$text_result,$font,$bg_col,$`,$name);
	    $form->{$text_result}->insert($font,$color_listen,$bg_col,$&);
	    insert_colored_result ($form,$text_result,$font,$bg_col,$',$name);
	    last;
	};

	/open\w*/i && do
	{
	    insert_colored_result ($form,$text_result,$font,$bg_col,$`,$name);
	    $form->{$text_result}->insert($font,$color_open,$bg_col,$&);
	    insert_colored_result ($form,$text_result,$font,$bg_col,$',$name);
	    last;
	};

	/connect\w*/i && do
	{
	    insert_colored_result ($form,$text_result,$font,$bg_col,$`,$name);
	    $form->{$text_result}->insert($font,$color_open,$bg_col,$&);
	    insert_colored_result ($form,$text_result,$font,$bg_col,$',$name);
	    last;
	};

	/close\w*/i && do
	{
	    insert_colored_result ($form,$text_result,$font,$bg_col,$`,$name);
	    $form->{$text_result}->insert($font,$color_close,$bg_col,$&);
	    insert_colored_result ($form,$text_result,$font,$bg_col,$',$name);
	    last;
	};

	/tcp|stream/i && do
	{
	    insert_colored_result ($form,$text_result,$font,$bg_col,$`,$name);
	    $form->{$text_result}->insert($font,$color_tcp,$bg_col,$&);
	    insert_colored_result ($form,$text_result,$font,$bg_col,$',$name);
	    last;
	};

	/udp|dgram/i && do
	{
	    insert_colored_result ($form,$text_result,$font,$bg_col,$`,$name);
	    $form->{$text_result}->insert($font,$color_udp,$bg_col,$&);
	    insert_colored_result ($form,$text_result,$font,$bg_col,$',$name);
	    last;
	};

	/raw/i && do
	{
	    insert_colored_result ($form,$text_result,$font,$bg_col,$`,$name);
	    $form->{$text_result}->insert($font,$color_raw,$bg_col,$&);
	    insert_colored_result ($form,$text_result,$font,$bg_col,$',$name);
	    last;
	};

	$form->{$text_result}->insert($font,$color_base,$bg_col,$ent);
    }
    return $true;
}
# End of the sub insert_colored_result


#==============================================================================
# Set actual colors from the config file
#==============================================================================
sub set_actual_colors_from_gink_config
{

    # Config file colors
    my @cf_base;
    my @cf_ip;
    my @cf_num;
    my @cf_search;
    my @cf_listen;
    my @cf_open;
    my @cf_close;
    my @cf_tcp;
    my @cf_udp;
    my @cf_raw;
    my @cf_bg;

    # Default colors from the config file
    ($cf_base[0], $cf_base[1], $cf_base[2], undef)
	= split (':', Gnome::Config->get_string ('/gink/colors/base'));
    ($cf_ip[0], $cf_ip[1], $cf_ip[2], undef)
	= split (':', Gnome::Config->get_string ('/gink/colors/ip'));
    ($cf_num[0], $cf_num[1], $cf_num[2], undef)
	= split (':', Gnome::Config->get_string ('/gink/colors/num'));
    ($cf_search[0], $cf_search[1], $cf_search[2], undef)
	= split (':', Gnome::Config->get_string ('/gink/colors/search'));
    ($cf_listen[0], $cf_listen[1], $cf_listen[2], undef)
	= split (':', Gnome::Config->get_string ('/gink/colors/listen'));
    ($cf_open[0], $cf_open[1], $cf_open[2], undef)
	= split (':', Gnome::Config->get_string ('/gink/colors/open'));
    ($cf_close[0], $cf_close[1], $cf_close[2], undef)
	= split (':', Gnome::Config->get_string ('/gink/colors/close'));
    ($cf_tcp[0], $cf_tcp[1], $cf_tcp[2], undef)
	= split (':', Gnome::Config->get_string ('/gink/colors/tcp'));
    ($cf_udp[0], $cf_udp[1], $cf_udp[2], undef)
	= split (':', Gnome::Config->get_string ('/gink/colors/udp'));
    ($cf_raw[0], $cf_raw[1], $cf_raw[2], undef)
	= split (':', Gnome::Config->get_string ('/gink/colors/raw'));
    ($cf_bg[0], $cf_bg[1], $cf_bg[2], undef)
	= split (':', Gnome::Config->get_string ('/gink/colors/bg'));

    # Set actual colors
    $color_close  = parse_color (@cf_close);
    $color_open   = parse_color (@cf_open);
    $color_num    = parse_color (@cf_num);
    $color_search = parse_color (@cf_search);
    $color_ip     = parse_color (@cf_ip);
    $color_listen = parse_color (@cf_listen);
    $color_tcp    = parse_color (@cf_tcp);
    $color_udp    = parse_color (@cf_udp);
    $color_raw    = parse_color (@cf_raw);
    $color_bg     = parse_color (@cf_bg);
    $color_base   = parse_color (@cf_base);

    return $true;
}
# End of sub set_actual_colors_from_gink_config

# parse the color
# ---------------
sub parse_color
{
    my @rgb = @_;
    my $color;
    $color->{ 'red' } = $rgb[0];
    $color->{ 'green' } = $rgb[1];
    $color->{ 'blue' } = $rgb[2];
    return $color;
}


1;

__END__

#==============================================================================
# Documentation
#==============================================================================
= pod = head1 NAME Gink - version 0.01 dim d .b�� c 8 12: 03:15 CET 2002
	No description
	= head1 SYNOPSIS use Gink;

To construct the window object and show it call Gtk->init;
my $window = app_gink->new;
$window->TOPLEVEL->show;
Gtk->main;

OR use the shorthand for the above calls app_gink->app_run;

= head1
      DESCRIPTION Unfortunately, the author has not yet written any documentation:-( = head1 AUTHOR
	  YLT,,, <yann \ @merlin >
	  = cut
