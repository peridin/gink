#!/usr/bin/perl -w
#
#    This file is part of Gink.
#
#    Gink is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#==============================================================================
#
# Subpropertybox_settings.pm
#
# UI class 'Subpropertybox_settings'
#
# Copyright 2003 Yann Le Thieis
# Contact: Yann Le Thieis - <lethieis@yahoo.fr>
#
# Pod description after the code
#
#==============================================================================
# $Id: Subpropertybox_settings.pm,v 1.2 2003/02/28 13:41:58 peredin Exp $
#==============================================================================
package Subpropertybox_settings;
#require 5.000; use strict 'vars', 'refs', 'subs';
require 5.000; use strict 'vars', 'subs';

my $true = 1;
my $false = 0;

# For checking the config file changed
my $rc;

# For the colors
my @color_base;
my @color_ip;
my @color_num;
my @color_search;
my @color_listen;
my @color_open;
my @color_close;
my @color_tcp;
my @color_udp;
my @color_raw;
my @color_bg;

# For the color labels
my $lb_base;
my $lb_ip;
my $lb_num;
my $lb_search;
my $lb_listen;
my $lb_open;
my $lb_close;
my $lb_tcp;
my $lb_udp;
my $lb_raw;
my $lb_bg;

# For the net tools
my $finger;
my $traceroute;
my $ping;
my $portscan;
my $tcpdump;
my $netstat;

BEGIN {
    use vars qw( 
                 @ISA
                 %fields
                 $PACKAGE
                 $VERSION
                 $AUTHOR
                 $DATE
                 $permitted_fields
             );
    use Finger;

    use Subcolorselectiondialog;

    # Existing signal handler modules
#    use src::Gink;

    # We need the Gnome bindings as well
#    use Gnome;

    # Uncomment the line below to enable gettext checking
#    use Glade::PerlSource;
    # Tell interpreter who we are inheriting from
    @ISA     = qw( propertybox_settings );
    # Uncomment the line below to enable gettext checking
#    @ISA      = qw( propertybox_settings Glade::PerlSource );
#    $PACKAGE = 'SubGink';
#    $VERSION = '0.01';
#    $AUTHOR  = 'YLT,,, <yann\@merlin>';
#    $DATE    = 'jeu jan  9 18:12:21 CET 2003';
    $permitted_fields = '_permitted_fields';
    # Inherit the AUTOLOAD dynamic methods from propertybox_settings
    *AUTOLOAD = \&propertybox_settings::AUTOLOAD;
} # End of sub BEGIN

%fields = (
# Insert any extra data access methods that you want to add to 
#   our inherited super-constructor (or overload)
    USERDATA    => undef,
    VERSION     => '0.01',
);

sub DESTROY {
    # This sub will be called on object destruction
    Gtk->main_quit;
} # End of sub DESTROY

#==============================================================================
#=== Below are the class constructors
#==============================================================================
sub new
{
    my $that  = shift;
    # Allow indirect constructor so that we can call eg. 
    #   $window1 = BusFrame->new; $window2 = $window1->new;
    my $class = ref($that) || $that;

    # Call our super-class constructor to get an object and reconsecrate it
    my $self = bless $that->SUPER::new(), $class;

    # Add our own data access methods to the inherited constructor
    my($element);
    foreach $element (keys %fields) {
        $self->{$permitted_fields}->{$element} = $fields{$element};
    }
    @{$self}{keys %fields} = values %fields;
    return $self;
} # End of sub new

sub app_run
{
    my ($class,$defprop,$local_finger,$local_traceroute) = @_;

    # Get finger config from config file
    my $fpath = Gnome::Config->get_string ('/gink/finger/path');
    my $fopt = Gnome::Config->get_string ('/gink/finger/options');
    # Get traceroute config from config file
    my $tpath = Gnome::Config->get_string ('/gink/traceroute/path');
    my $topt = Gnome::Config->get_string ('/gink/traceroute/options');

    Gnome->init('Gink', '0.01');
    # Uncomment the line below to enable gettext checking
#    $class->check_gettext_strings;
    my $window = $class->new;
    # Insert your subclass user data key/value pairs 
    $window->USERDATA({
#        'Key1'   => 'Value1',
#        'Key2'   => 'Value2',
#        'Key3'   => 'Value3',
		      });

    # Keep the config file state reference
    $rc = $defprop;

    # Default font
    my $font_name = Gnome::Config->get_string ('/gink/font/police');
    print "Gink: Settings...\n";
    $window->FORM->{'fontselection'}->set_font_name ($font_name);

    # Default colors
    ($color_base[0], $color_base[1], $color_base[2], $lb_base)
	= split (':', Gnome::Config->get_string ('/gink/colors/base'));
    ($color_ip[0], $color_ip[1], $color_ip[2], $lb_ip)
	= split (':', Gnome::Config->get_string ('/gink/colors/ip'));
    ($color_num[0], $color_num[1], $color_num[2], $lb_num)
	= split (':', Gnome::Config->get_string ('/gink/colors/num'));
    ($color_search[0], $color_search[1], $color_search[2], $lb_search)
	= split (':', Gnome::Config->get_string ('/gink/colors/search'));
    ($color_listen[0], $color_listen[1], $color_listen[2], $lb_listen)
	= split (':', Gnome::Config->get_string ('/gink/colors/listen'));
    ($color_open[0], $color_open[1], $color_open[2], $lb_open)
	= split (':', Gnome::Config->get_string ('/gink/colors/open'));
    ($color_close[0], $color_close[1], $color_close[2], $lb_close)
	= split (':', Gnome::Config->get_string ('/gink/colors/close'));
    ($color_tcp[0], $color_tcp[1], $color_tcp[2], $lb_tcp)
	= split (':', Gnome::Config->get_string ('/gink/colors/tcp'));
    ($color_udp[0], $color_udp[1], $color_udp[2], $lb_udp)
	= split (':', Gnome::Config->get_string ('/gink/colors/udp'));
    ($color_raw[0], $color_raw[1], $color_raw[2], $lb_raw)
	= split (':', Gnome::Config->get_string ('/gink/colors/raw'));
    ($color_bg[0], $color_bg[1], $color_bg[2], $lb_bg)
	= split (':', Gnome::Config->get_string ('/gink/colors/bg'));

    my $color = parse_color (@color_ip);
    $window->FORM->{'text_color_base'}->insert(undef, parse_color(@color_base), undef, $lb_base);
    $window->FORM->{'text_color_ip'}->insert(undef, parse_color(@color_ip), undef, $lb_ip);
    $window->FORM->{'text_color_num'}->insert(undef,parse_color(@color_num), undef, $lb_num);
    $window->FORM->{'text_color_search'}->insert(undef, parse_color(@color_search), undef, $lb_search);
    $window->FORM->{'text_color_listen'}->insert(undef, parse_color(@color_listen), undef, $lb_listen);
    $window->FORM->{'text_color_open'}->insert(undef, parse_color(@color_open), undef, $lb_open);
    $window->FORM->{'text_color_close'}->insert(undef, parse_color(@color_close), undef, $lb_close);
    $window->FORM->{'text_color_tcp'}->insert(undef, parse_color(@color_tcp), undef, $lb_tcp);
    $window->FORM->{'text_color_udp'}->insert(undef, parse_color(@color_udp), undef, $lb_udp);
    $window->FORM->{'text_color_raw'}->insert(undef, parse_color(@color_raw), undef, $lb_raw);
    $window->FORM->{'text_color_bg'}->insert(undef, undef, parse_color(@color_bg), $lb_bg);

    # Here are the Finger object specifics
    $finger = $local_finger;
    $window->FORM->{'checkbutton_finger_setopt_s'}->set_active ($true) if ($fopt =~ /-s/);
    $window->FORM->{'checkbutton_finger_setopt_l'}->set_active ($true) if ($fopt =~ /-l/);
    $window->FORM->{'checkbutton_finger_setopt_p'}->set_active ($true) if ($fopt =~ /-p/);
    $window->FORM->{'checkbutton_finger_setopt_m'}->set_active ($true) if ($fopt =~ /-m/);
    $window->FORM->{'entry_set_finger_cmd'}->set_text ($fpath);
    $window->FORM->{'label_finger_setopt_cmd'}->set_text ($fpath.$finger->cmd.$fopt);

    # Here are the Traceroute object specifics
    $traceroute = $local_traceroute;
    $window->FORM->{'checkbutton_traceroute_verbose_opt'}->set_active ($true) if ($topt =~ /-v/);
    $window->FORM->{'checkbutton_traceroute_resolve_opt'}->set_active ($true) if ($topt =~ /-n/);
    $window->FORM->{'checkbutton_traceroute_fragment_opt'}->set_active ($true) if ($topt =~ /-F/);
    $window->FORM->{'checkbutton_traceroute_debug_opt'}->set_active ($true) if ($topt =~ /-d/);
    $window->FORM->{'checkbutton_traceroute_hops_opt'}->set_active ($true) if ($topt =~ /-m/);
    $window->FORM->{'checkbutton_traceroute_ttl_opt'}->set_active ($true) if ($topt =~ /-l/);
    if ($topt =~ /-I/)
    {
	$window->FORM->{'radiobutton_traceroute_icmp_opt'}->set_active ($true);
    }
    else
    {
	$window->FORM->{'radiobutton_traceroute_udp_opt'}->set_active ($true);
    }
    if ($topt =~ /-t 16/)
    {
	$window->FORM->{'radiobutton_traceroute_low_opt'}->set_active ($true);
    }
    elsif ($topt =~ /-t 8/)
    {
	$window->FORM->{'radiobutton_traceroute_high_opt'}->set_active ($true);
    }
    else
    {
	$window->FORM->{'radiobutton_traceroute_default_opt'}->set_active ($true);
    }
    $window->FORM->{'entry_set_traceroute_cmd'}->set_text ($tpath);
    $window->FORM->{'label_traceroute_command_opt'}->set_text ($tpath.$traceroute->cmd.$topt);


    # For now the user don't change any settings in this box, so...
    $window->FORM->{'propertybox_settings'}->set_modified ($false);

    $window->TOPLEVEL->show;
#    my $window2 = $window->new;
#    $window2->TOPLEVEL->show;

    Gtk->main;

    # Uncomment the line below to enable gettext checking
#    $window->write_gettext_strings("__", '/home/yann/Projets/gink/src/Gink.pot');
    $window->TOPLEVEL->destroy;

    return $window;
}
# End of sub run


#==============================================================================
# Signal handler for applying modifications
#==============================================================================
sub on_propertybox_settings_apply
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_propertybox_settings_apply";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};


    # Get the new settings and applying it to the config file
    # -------------------------------------------------------

    # Save result font in the config file
    my $font_name = $form->{'fontselection'}->get_font_name ();
    Gnome::Config->set_string ('/gink/font/police', $font_name);

    # Save result colors in the config file
    Gnome::Config->set_string (
			       '/gink/colors/base',
			       join (':', ($color_base[0], $color_base[1], $color_base[2], $lb_base))
			      );
    Gnome::Config->set_string (
			       '/gink/colors/ip',
			       join (':', ($color_ip[0], $color_ip[1], $color_ip[2], $lb_ip))
			      );
    Gnome::Config->set_string (
			       '/gink/colors/num',
			       join (':', ($color_num[0], $color_num[1], $color_num[2], $lb_num))
			      );
    Gnome::Config->set_string (
			       '/gink/colors/search',
			       join (':', ($color_search[0], $color_search[1], $color_search[2], $lb_search))
			      );
    Gnome::Config->set_string (
			       '/gink/colors/listen',
			       join (':', ($color_listen[0], $color_listen[1], $color_listen[2], $lb_listen))
			      );
    Gnome::Config->set_string (
			       '/gink/colors/open',
			       join (':', ($color_open[0], $color_open[1], $color_open[2], $lb_open))
			      );
    Gnome::Config->set_string (
			       '/gink/colors/close',
			       join (':', ($color_close[0], $color_close[1], $color_close[2], $lb_close))
			      );
    Gnome::Config->set_string (
			       '/gink/colors/tcp',
			       join (':', ($color_tcp[0], $color_tcp[1], $color_tcp[2], $lb_tcp))
			      );
    Gnome::Config->set_string (
			       '/gink/colors/udp',
			       join(':', ($color_udp[0], $color_udp[1], $color_udp[2], $lb_udp))
			      );
    Gnome::Config->set_string (
			       '/gink/colors/raw',
			       join(':', ($color_raw[0], $color_raw[1], $color_raw[2], $lb_raw))
			      );
    Gnome::Config->set_string (
			       '/gink/colors/bg',
			       join(':', ($color_bg[0], $color_bg[1], $color_bg[2], $lb_bg))
			      );

    # Save Finger settings (options and binpath)
    my $options = "";
    $options .= " -s" if ( $form->{'checkbutton_finger_setopt_s'}->active );
    $options .= " -l" if ( $form->{'checkbutton_finger_setopt_l'}->active );
    $options .= " -p" if ( $form->{'checkbutton_finger_setopt_p'}->active );
    $options .= " -m" if ( $form->{'checkbutton_finger_setopt_m'}->active );
    Gnome::Config->set_string ('/gink/finger/options', $options);

    # Save Traceroute settings (options and binpath)
    my $options = "";
    $options .= " -v" if ( $form->{'checkbutton_traceroute_verbose_opt'}->active );
    if ( $form->{'checkbutton_traceroute_hops_opt'}->active )
    {
	my $hops = $form->{'entry_traceroute_hops_opt'}->get_text ();
	if ($hops != 30)
	{
	    $options .= " -m";
	}
    }
    $options .= " -n" if ( $form->{'checkbutton_traceroute_resolve_opt'}->active );
    $options .= " -F" if ( $form->{'checkbutton_traceroute_fragment_opt'}->active );
    $options .= " -d" if ( $form->{'checkbutton_traceroute_socket_opt'}->active );
    $options .= " -l" if ( $form->{'checkbutton_traceroute_ttl_opt'}->active );
    if ( $form->{'radiobutton_traceroute_icmp_opt'}->active )
    {
	$options .= " -I";
    }
    elsif ( $form->{'radiobutton_traceroute_udp_opt'}->active )
    {
	my $port = $form->{'entry_traceroute_udp_opt'}->get_text ();
	if ($port != 33434)
	{
	    $options .= " -p $port";
	}
    }
    if ( $form->{'radiobutton_traceroute_low_opt'}->active )
    {
	$options .= " -t 16";
    }
    elsif ( $form->{'radiobutton_traceroute_high_opt'}->active )
    {
	$options .= " -t 8";
    }
    Gnome::Config->set_string ('/gink/traceroute/options', $options);


    my $path = $form->{'entry_set_finger_cmd'}->get_text;
    $finger->set_path ($path);
    Gnome::Config->set_string ('/gink/finger/path', $path);

    $form->{'label_finger_setopt_cmd'}->set_text ($path.$finger->cmd.$options);

    # To be sure the config file is up to date
    Gnome::Config->sync_file ('/gink');

    # The config file has been changed
    $$rc = $true;

    return $true;
}
# End of sub on_propertybox_settings_apply



#===============================================================================
# On focus on the font result selection, activate apply button
#===============================================================================
sub on_fontselection_set_focus_child
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_fontselection_focus";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    $form->{'propertybox_settings'}->set_modified ($true);
    return $true;
}


#==============================================================================
# Signal handlers for launching color selection
#==============================================================================
sub on_button_color_base_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_button_color_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    Subcolorselectiondialog->app_run(\@color_base);
    $form->{'text_color_base'}->delete_text;
    $form->{'text_color_base'}->insert(undef, parse_color(@color_base), undef, $lb_base);

    $form->{'propertybox_settings'}->set_modified ($true);
    return $true;
}

sub on_button_color_bg_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_button_color_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    Subcolorselectiondialog->app_run(\@color_bg);
    $form->{'text_color_bg'}->delete_text;
    $form->{'text_color_bg'}->insert(undef, undef, parse_color(@color_bg), $lb_bg);

    $form->{'propertybox_settings'}->set_modified ($true);
    return $true;
}

sub on_button_color_close_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_button_color_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    Subcolorselectiondialog->app_run(\@color_close);
    $form->{'text_color_close'}->delete_text;
    $form->{'text_color_close'}->insert(undef, parse_color(@color_close), undef, $lb_close);

    $form->{'propertybox_settings'}->set_modified ($true);
    return $true;
}

sub on_button_color_ip_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_button_color_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    Subcolorselectiondialog->app_run(\@color_ip);
    $form->{'text_color_ip'}->delete_text;
    $form->{'text_color_ip'}->insert(undef, parse_color(@color_ip), undef, $lb_ip);

    $form->{'propertybox_settings'}->set_modified ($true);
    return $true;
}

sub on_button_color_listen_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_button_color_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    Subcolorselectiondialog->app_run(\@color_listen);
    $form->{'text_color_listen'}->delete_text;
    $form->{'text_color_listen'}->insert(undef, parse_color(@color_listen), undef, $lb_listen);

    $form->{'propertybox_settings'}->set_modified ($true);
    return $true;
}

sub on_button_color_num_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_button_color_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    Subcolorselectiondialog->app_run(\@color_num);
    $form->{'text_color_num'}->delete_text;
    $form->{'text_color_num'}->insert(undef, parse_color(@color_num), undef, $lb_num);

    $form->{'propertybox_settings'}->set_modified ($true);
    return $true;
}

sub on_button_color_open_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_button_color_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    Subcolorselectiondialog->app_run(\@color_open);
    $form->{'text_color_open'}->delete_text;
    $form->{'text_color_open'}->insert(undef, parse_color(@color_open), undef, $lb_open);

    $form->{'propertybox_settings'}->set_modified ($true);
    return $true;
}

sub on_button_color_raw_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_button_color_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    Subcolorselectiondialog->app_run(\@color_raw);
    $form->{'text_color_raw'}->delete_text;
    $form->{'text_color_raw'}->insert(undef, parse_color(@color_raw), undef, $lb_raw);

    $form->{'propertybox_settings'}->set_modified ($true);
    return $true;
}

sub on_button_color_search_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_button_color_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    Subcolorselectiondialog->app_run(\@color_search);
    $form->{'text_color_search'}->delete_text;
    $form->{'text_color_search'}->insert(undef, parse_color(@color_search), undef, $lb_search);

    $form->{'propertybox_settings'}->set_modified ($true);
    return $true;
}

sub on_button_color_tcp_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_button_color_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    Subcolorselectiondialog->app_run(\@color_tcp);
    $form->{'text_color_tcp'}->delete_text;
    $form->{'text_color_tcp'}->insert(undef, parse_color(@color_tcp), undef, $lb_tcp);

    $form->{'propertybox_settings'}->set_modified ($true);
    return $true;
}

sub on_button_color_udp_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_button_color_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    Subcolorselectiondialog->app_run(\@color_udp);
    $form->{'text_color_udp'}->delete_text;
    $form->{'text_color_udp'}->insert(undef, parse_color(@color_udp), undef, $lb_udp);

    $form->{'propertybox_settings'}->set_modified ($true);
    return $true;
}


#==============================================================================
# Signal handler for getting default finger config in the config file
#==============================================================================
sub on_button_get_finger_config_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_button_get_finger_config_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    # Get the defaults in the config file
    my $opt = Gnome::Config->get_string ('/gink/finger/options');
    my $path = Gnome::Config->get_string ('/gink/finger/path');

    # Applying these to the UI
    if ($opt =~ /-s/)
    {
	$form->{'checkbutton_finger_setopt_s'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_finger_setopt_s'}->set_active ($false);
    }
    if ($opt =~ /-l/)
    {
	$form->{'checkbutton_finger_setopt_l'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_finger_setopt_l'}->set_active ($false);
    }
    if ($opt =~ /-p/)
    {
	$form->{'checkbutton_finger_setopt_p'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_finger_setopt_p'}->set_active ($false);
    }
    if ($opt =~ /-m/)
    {
	$form->{'checkbutton_finger_setopt_m'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_finger_setopt_m'}->set_active ($false);
    }
    $form->{'entry_set_finger_cmd'}->set_text ($path);
    $form->{'label_finger_setopt_cmd'}->set_text ($path.$finger->cmd.$opt);

    return $true;
}
# End of sub on_button_get_finger_config_clicked


#==============================================================================
# Signal handler for getting actual finger options
#==============================================================================
sub on_button_get_finger_options_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_button_get_finger_options_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    # Applying actual finger options to the UI
    if ($finger->options =~ /-s/)
    {
	$form->{'checkbutton_finger_setopt_s'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_finger_setopt_s'}->set_active ($false);
    }
    if ($finger->options =~ /-l/)
    {
	$form->{'checkbutton_finger_setopt_l'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_finger_setopt_l'}->set_active ($false);
    }
    if ($finger->options =~ /-p/)
    {
	$form->{'checkbutton_finger_setopt_p'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_finger_setopt_p'}->set_active ($false);
    }
    if ($finger->options =~ /-m/)
    {
	$form->{'checkbutton_finger_setopt_m'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_finger_setopt_m'}->set_active ($false);
    }
    $form->{'entry_set_finger_cmd'}->set_text ($finger->path);
    $form->{'label_finger_setopt_cmd'}->set_text ($finger->path.$finger->cmd.$finger->options);

    # To activate the apply button (or activate apply when OK button clicked)
    $form->{'propertybox_settings'}->changed;

    return $true;
}
# End of sub on_button_get_finger_options_clicked


#===============================================================================
# Finger update on each option checkbutton click
#===============================================================================
sub on_checkbutton_finger_setopt_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_checkbutton_finger_setopt_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    # Get clicked options
    my $options = "";
    $options .= " -s" if ( $form->{'checkbutton_finger_setopt_s'}->active );
    $options .= " -l" if ( $form->{'checkbutton_finger_setopt_l'}->active );
    $options .= " -p" if ( $form->{'checkbutton_finger_setopt_p'}->active );
    $options .= " -m" if ( $form->{'checkbutton_finger_setopt_m'}->active );
    my $path = $form->{'entry_set_finger_cmd'}->get_text;

    # Update the command example
    $form->{'label_finger_setopt_cmd'}->set_text ($path.$finger->cmd.$options);

    # To activate the apply button (or activate apply when OK button clicked)
    $form->{'propertybox_settings'}->changed;

    return $true;
}
# End of sub on_checkbutton_finger_setopt_clicked

#==============================================================================
# Signal handler for getting default traceroute config in the config file
#==============================================================================
sub on_button_get_traceroute_config_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_button_get_traceroute_config_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    # Get the defaults in the config file
    my $opt = Gnome::Config->get_string ('/gink/traceroute/options');
    my $path = Gnome::Config->get_string ('/gink/traceroute/path');

    # Applying these to the UI
    if ($opt =~ /-v/)
    {
	$form->{'checkbutton_traceroute_verbose_opt'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_traceroute_verbose_opt'}->set_active ($false);
    }
    if ($opt =~ /-m/)
    {
	$form->{'checkbutton_traceroute_hops_opt'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_traceroute_hops_opt'}->set_active ($false);
    }
    if ($opt =~ /-n/)
    {
	$form->{'checkbutton_traceroute_resolve_opt'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_traceroute_resolve_opt'}->set_active ($false);
    }
    if ($opt =~ /-F/)
    {
	$form->{'checkbutton_traceroute_fragment_opt'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_traceroute_fragment_opt'}->set_active ($false);
    }
    if ($opt =~ /-d/)
    {
	$form->{'checkbutton_traceroute_socket_opt'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_traceroute_socket_opt'}->set_active ($false);
    }
    if ($opt =~ /-l/)
    {
	$form->{'checkbutton_traceroute_ttl_opt'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_traceroute_ttl_opt'}->set_active ($false);
    }
    if ($opt =~ /-I/)
    {
	$form->{'radiobutton_traceroute_icmp_opt'}->set_active ($true);
    }
    else
    {
	$form->{'radiobutton_traceroute_udp_opt'}->set_active ($true);
    }
    if ($opt =~ /-t 16/)
    {
	$form->{'radiobutton_traceroute_low_opt'}->set_active ($true);
    }
    elsif ($opt =~ /-t 8/)
    {
	$form->{'radiobutton_traceroute_high_opt'}->set_active ($true);
    }
    else
    {
	$form->{'radiobutton_traceroute_default_opt'}->set_active ($true);
    }
    $form->{'entry_set_traceroute_cmd'}->set_text ($path);
    $form->{'label_traceroute_command_opt'}->set_text ($path.$traceroute->cmd.$opt);

    return $true;
}
# End of sub on_button_get_traceroute_config_clicked

#==============================================================================
# Signal handler for getting default traceroute config in the config file
#==============================================================================
sub on_button_get_traceroute_config_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_button_get_traceroute_config_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    # Get the defaults in the config file
    my $opt = Gnome::Config->get_string ('/gink/traceroute/options');
    my $path = Gnome::Config->get_string ('/gink/traceroute/path');

    # Applying these to the UI
    if ($opt =~ /-v/)
    {
	$form->{'checkbutton_traceroute_verbose_opt'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_traceroute_verbose_opt'}->set_active ($false);
    }
    if ($opt =~ /-m/)
    {
	$form->{'checkbutton_traceroute_hops_opt'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_traceroute_hops_opt'}->set_active ($false);
    }
    if ($opt =~ /-n/)
    {
	$form->{'checkbutton_traceroute_resolve_opt'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_traceroute_resolve_opt'}->set_active ($false);
    }
    if ($opt =~ /-F/)
    {
	$form->{'checkbutton_traceroute_fragment_opt'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_traceroute_fragment_opt'}->set_active ($false);
    }
    if ($opt =~ /-d/)
    {
	$form->{'checkbutton_traceroute_socket_opt'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_traceroute_socket_opt'}->set_active ($false);
    }
    if ($opt =~ /-l/)
    {
	$form->{'checkbutton_traceroute_ttl_opt'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_traceroute_ttl_opt'}->set_active ($false);
    }
    if ($opt =~ /-I/)
    {
	$form->{'radiobutton_traceroute_icmp_opt'}->set_active ($true);
    }
    else
    {
	$form->{'radiobutton_traceroute_udp_opt'}->set_active ($true);
    }
    if ($opt =~ /-t 16/)
    {
	$form->{'radiobutton_traceroute_low_opt'}->set_active ($true);
    }
    elsif ($opt =~ /-t 8/)
    {
	$form->{'radiobutton_traceroute_high_opt'}->set_active ($true);
    }
    else
    {
	$form->{'radiobutton_traceroute_default_opt'}->set_active ($true);
    }
    $form->{'entry_set_traceroute_cmd'}->set_text ($path);
    $form->{'label_traceroute_command_opt'}->set_text ($path.$traceroute->cmd.$opt);

    return $true;
}
# End of sub on_button_get_traceroute_config_clicked


#==============================================================================
# Signal handler for getting actual traceroute options
#==============================================================================
sub on_button_get_traceroute_options_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_button_get_traceroute_options_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    # Applying actual traceroute options to the UI
    if ($traceroute->options =~ /-v/)
    {
	$form->{'checkbutton_traceroute_verbose_opt'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_traceroute_verbose_opt'}->set_active ($false);
    }
    if ($traceroute->options =~ /-m/)
    {
	$form->{'checkbutton_traceroute_hops_opt'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_traceroute_hops_opt'}->set_active ($false);
    }
    if ($traceroute->options =~ /-n/)
    {
	$form->{'checkbutton_traceroute_resolve_opt'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_traceroute_resolve_opt'}->set_active ($false);
    }
    if ($traceroute->options =~ /-F/)
    {
	$form->{'checkbutton_traceroute_fragment_opt'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_traceroute_fragment_opt'}->set_active ($false);
    }
    if ($traceroute->options =~ /-d/)
    {
	$form->{'checkbutton_traceroute_socket_opt'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_traceroute_socket_opt'}->set_active ($false);
    }
    if ($traceroute->options =~ /-l/)
    {
	$form->{'checkbutton_traceroute_ttl_opt'}->set_active ($true);
    }
    else
    {
	$form->{'checkbutton_traceroute_ttl_opt'}->set_active ($false);
    }
    if ($traceroute->options =~ /-I/)
    {
	$form->{'radiobutton_traceroute_icmp_opt'}->set_active ($true);
    }
    else
    {
	$form->{'radiobutton_traceroute_udp_opt'}->set_active ($true);
    }
    if ($traceroute->options =~ /-t 16/)
    {
	$form->{'radiobutton_traceroute_low_opt'}->set_active ($true);
    }
    elsif ($traceroute->options =~ /-t 8/)
    {
	$form->{'radiobutton_traceroute_high_opt'}->set_active ($true);
    }
    else
    {
	$form->{'radiobutton_traceroute_default_opt'}->set_active ($true);
    }
    $form->{'entry_set_traceroute_cmd'}->set_text ($traceroute->path);
    $form->{'label_traceroute_command_opt'}->set_text ($traceroute->path.$traceroute->cmd.$traceroute->options);

    # To activate the apply button (or activate apply when OK button clicked)
    $form->{'propertybox_settings'}->changed;

    return $true;
}
# End of sub on_button_get_traceroute_options_clicked

#===============================================================================
# Traceroute update on each option checkbutton click
#===============================================================================
sub on_traceroute_button_options_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_traceroute_button_options_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    # Get clicked options
    my $options = "";
    $options .= " -v" if ( $form->{'checkbutton_traceroute_verbose_opt'}->active );
    $options .= " -m" if ( $form->{'checkbutton_traceroute_hops_opt'}->active );
    $options .= " -n" if ( $form->{'checkbutton_traceroute_resolve_opt'}->active );
    $options .= " -F" if ( $form->{'checkbutton_traceroute_fragment_opt'}->active );
    $options .= " -d" if ( $form->{'checkbutton_traceroute_socket_opt'}->active );
    $options .= " -l" if ( $form->{'checkbutton_traceroute_ttl_opt'}->active );
    if ( $form->{'radiobutton_traceroute_icmp_opt'}->active )
    {
	$options .= " -I" ;
    }
    else
    {
	my $port = $form->{'entry_traceroute_udp_opt'}->get_text ();
	if ($port != 33434)
	{
	    $options .= " -p ".$port;
	}
    }
    if ( $form->{'radiobutton_traceroute_low_opt'}->active )
    {
	$options .= " -t 16";
    }
    elsif ( $form->{'radiobutton_traceroute_high_opt'}->active )
    {
	$options .= " -t 8";
    }

    my $path = $form->{'entry_set_traceroute_cmd'}->get_text;

    # Update the command example
    $form->{'label_traceroute_command_opt'}->set_text ($path.$traceroute->cmd.$options);

    # To activate the apply button (or activate apply when OK button clicked)
    $form->{'propertybox_settings'}->changed;

    return $true;
}
# End of sub on_traceroute_button_options_clicked

#===============================================================================
# parse the color
#===============================================================================
sub parse_color
{
    my @rgb = @_;
    my $color;
    $color->{ 'red' } = $rgb[0];
    $color->{ 'green' } = $rgb[1];
    $color->{ 'blue' } = $rgb[2];
    return $color;
}

1;

__END__

#===============================================================================
#==== Documentation
#===============================================================================
=pod

=head1 NAME

SubGink - version 0.01 jeu jan  9 18:12:21 CET 2003

No description

=head1 SYNOPSIS

 use SubGink;

 To construct the window object and show it call
 
 Gtk->init;
 my $window = Subapp_gink->new;
 $window->TOPLEVEL->show;
 Gtk->main;
 
 OR use the shorthand for the above calls
 
 Subapp_gink->app_run;

=head1 DESCRIPTION

Unfortunately, the author has not yet written any documentation :-(

=head1 AUTHOR

YLT,,, <yann\@merlin>

=cut
