#!/usr/bin/perl -w
#
#
#    This file is part of Gink.
#
#    Gink is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#==============================================================================
#
# Subcolorselectiondialog.pm
#
# UI class 'Subcolorselectiondialog'
#
# Copyright 2003 Yann Le Thieis
# Contact: Yann Le Thieis - <lethieis@yahoo.fr>
#
# Pod description after the code
#
#==============================================================================
# $Id: Subcolorselectiondialog.pm,v 1.1.1.1 2003/01/24 08:35:21 peredin Exp $
#==============================================================================
package Subcolorselectiondialog;
#require 5.000; use strict 'vars', 'refs', 'subs';
require 5.000; use strict 'vars', 'subs';

my $true = 1;
my $false = 0;

# reference to the color to change
my $rc;

BEGIN {
    use vars qw( 
                 @ISA
                 %fields
                 $PACKAGE
                 $VERSION
                 $AUTHOR
                 $DATE
                 $permitted_fields
             );

    # Existing signal handler modules
#    use src::Gink;
    # We need the Gnome bindings as well
#    use Gnome;
    # Uncomment the line below to enable gettext checking
#    use Glade::PerlSource;
    # Tell interpreter who we are inheriting from
    @ISA     = qw( colorselectiondialog );
    # Uncomment the line below to enable gettext checking
#    @ISA      = qw( colorselectiondialog Glade::PerlSource );
#    $PACKAGE = 'SubGink';
#    $VERSION = '0.01';
#    $AUTHOR  = 'YLT,,, <yann\@merlin>';
#    $DATE    = 'jeu jan  9 18:12:21 CET 2003';
    $permitted_fields = '_permitted_fields';             
    # Inherit the AUTOLOAD dynamic methods from colorselectiondialog
    *AUTOLOAD = \&colorselectiondialog::AUTOLOAD;
} # End of sub BEGIN

%fields = (
# Insert any extra data access methods that you want to add to 
#   our inherited super-constructor (or overload)
    USERDATA    => undef,
    VERSION     => '0.01',
);

sub DESTROY {
    # This sub will be called on object destruction
} # End of sub DESTROY

#==============================================================================
#=== Below are the class constructors
#==============================================================================
sub new {
    my $that  = shift;
    # Allow indirect constructor so that we can call eg. 
    #   $window1 = BusFrame->new; $window2 = $window1->new;
    my $class = ref($that) || $that;

    # Call our super-class constructor to get an object and reconsecrate it
    my $self = bless $that->SUPER::new(), $class;

    # Add our own data access methods to the inherited constructor
    my($element);
    foreach $element (keys %fields) {
        $self->{$permitted_fields}->{$element} = $fields{$element};
    }
    @{$self}{keys %fields} = values %fields;
    return $self;
} # End of sub new

sub app_run {
    my ($class, $ref_color) = @_;

    Gnome->init('Gink', '0.01');
    # Uncomment the line below to enable gettext checking
#    $class->check_gettext_strings;
    my $window = $class->new;
    # Insert your subclass user data key/value pairs 
    $window->USERDATA({
#        'Key1'   => 'Value1',
#        'Key2'   => 'Value2',
#        'Key3'   => 'Value3',
    });
    $window->TOPLEVEL->show;
#    my $window2 = $window->new;
#    $window2->TOPLEVEL->show;

    # Keep the reference to the color
    $rc = $ref_color;
    my $red = $$ref_color[0] / 65535.0;
    my $green = $$ref_color[1] / 65535.0;
    my $blue = $$ref_color[2] / 65535.0;

    # Set the color selection
    $window->FORM->{'colorselectiondialog'}->colorsel->set_color ($red,$green,$blue);

    Gtk->main;
    # Uncomment the line below to enable gettext checking
#    $window->write_gettext_strings("__", '/home/yann/Projets/gink/src/Gink.pot');
    $window->TOPLEVEL->destroy;

    return $window;
}


#==============================================================================
# Signal handler for the cancel action in the color selection dialog
#==============================================================================
sub on_cancel_button_color_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_cancel_button_color_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    Gtk->main_quit;
    return $true;
}


#==============================================================================
# Signal handler for the ok action in the color selection dialog
#==============================================================================
sub on_ok_button_color_clicked
{
    my ($class, $data, $object, $instance, $event) = @_;
    my $me = __PACKAGE__."->on_ok_button_color_clicked";
    # Get ref to hash of all widgets on our form
    my $form = $__PACKAGE__::all_forms->{$instance};

    # Get the new color and apply it in the gink config file
    my @new_color = $form->{'colorselectiondialog'}->colorsel->get_color;

    # change the color of the ref color
    $$rc[0] = int ($new_color[0] * 65535.0);
    $$rc[1] = int ($new_color[1] * 65535.0);
    $$rc[2] = int ($new_color[2] * 65535.0);

    Gtk->main_quit;
    return $true;
}

1;

__END__
