#!/usr/bin/perl -w
#
#    This file is part of Gink.
#
#    Gink is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#==============================================================================
#
# Tcpdump.pm
#
# UI class 'Tcpdump'
# This is a sub class of Nettool
#
# Copyright 2003 Yann Le Thieis
# Contact:  Yann Le Thieis - <lethieis@yahoo.fr>
#
# Pod description after the code
#
#==============================================================================
# $Id: Tcpdump.pm,v 1.1.1.1 2003/01/24 08:35:21 peredin Exp $
#==============================================================================
package Tcpdump;

require 5.000;
use strict 'vars', 'refs', 'subs';

BEGIN
{
    use lib "./src";
    use Nettool;
    our @ISA = ("Nettool");
}

my $true = 1;
my $false = 0;

#==============================================================================
# Creation of a new tcpdump tool
#==============================================================================
sub new_nettool
{
    my ($class, $path, $cmd) = @_;
    my $tcpdump = $class->new ($path,$cmd);
    $tcpdump->{'count'} = 0;
    $tcpdump->{'count_type'} = "";
    return $tcpdump;
}


#==============================================================================
# Initializations
#==============================================================================
sub inits
{
    my ($tcpdump,$params,$count,$count_type) = @_;
    $tcpdump->{'parameters'} = $params;
    $tcpdump->{'count'} = $count;
    $tcpdump->{'count_type'} = $count_type;

    # just for now! Has to be changed
    $tcpdump->{'names'} = `hostname`;

    return $true;
}


# =============================================================================
# == Actualizations of tcpdump
# =============================================================================
sub actualize
{
    my ($tcpdump,$inc) = @_;
    $tcpdump->{'process_line'} += $inc;

    # Calculate process bar
    my $pbar = $tcpdump->{'process_state'};

 SWITCH: for ( $tcpdump->{'count_type'} )
    {	
	/packets/ && do
	{
	    $pbar = $tcpdump->{'process_line'} / ($tcpdump->{'count'} + 1);
	    ($pbar > 1) && ($pbar = 1);
	    last;
	};

	/kb/ && do
	{
	    my $sizefile = (-s "/tmp/tmp_tcpdump");
	    $pbar = $sizefile / ( $tcpdump->{'count'} * 1024 );
	    if ($pbar > 1)
	    {
		$pbar = 0;
		return $false;
	    }
	    last;
	};

	/seconds/ && do
	{
	    my $time = time();
	    $pbar = ($time - $tcpdump->{'start_time'}) / $tcpdump->{'count'};
	    if ($pbar > 1)
	    {
		$pbar = 0;
		return $false;
	    }
	    last;
	};
    }
    $tcpdump->{'process_state'} = $pbar;

    return $true;
}
# End of sub actualize

1;

__END__
