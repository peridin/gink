#!/usr/bin/perl -w
#
#    This file is part of Gink.
#
#    Gink is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#==============================================================================
#
# Nettool.pm
#
# UI class 'Nettool' (version 0.01)
# This is the 'Nettool' class
# This is a class for all net oriented object tool used by gink
#
# Copyright 2003 Yann Le Thieis
# Contact: Yann Le Thieis - <lethieis@yahoo.fr>
#
# Pod description after the code
#
#==============================================================================
# $Id: Nettool.pm,v 1.1.1.1 2003/01/24 08:35:21 peredin Exp $
#==============================================================================
package Nettool;
require 5.000;
use strict 'vars', 'refs', 'subs';

# For the gnome gink config file
use Gnome;

my @liste;

BEGIN
{
    @liste = ();
}

my $true = 1;
my $false = 0;

#==============================================================================
# Creation of a new net tool
#==============================================================================
sub new
{
    my ($class, $cmd) = @_;
    my $nettool = {};

    # For the background process
    $nettool->{'start_time'} = 0;

    # For the use of the result background process
    $nettool->{'process_line'} = 1;

    # Control of the process state
    $nettool->{'ready'} = $true;

    # For the background process
    $nettool->{'path'} = Gnome::Config->get_string ("/gink/$cmd/path");
    $nettool->{'cmd'} = $cmd;
    $nettool->{'options'} = Gnome::Config->get_string ("/gink/$cmd/options");
    $nettool->{'parameters'} = "";
    $nettool->{'names'} = "";

    # For the state of a launching process
    $nettool->{'process_state'} = 0;

    bless ($nettool,$class);
    push (@liste, $nettool);
    return $nettool;
}


#==============================================================================
# Initializations
#==============================================================================
sub inits
{
    my ($nettool,$names,$params) = @_;
    $nettool->{'parameters'} = $params;
    $nettool->{'names'} = $names;

    return $true;
}


#==============================================================================
#== Start the nettool background process
#==============================================================================
sub start
{
    my ($nettool) = @_;

    $nettool->{'process_line'} = 1;
    $nettool->{'ready'} = $false;
    $nettool->{'start_time'} = time;

    # Launch background processing
    my $path = $nettool->{'path'};
    my $cmd = $nettool->{'cmd'};
    my $options = $nettool->{'options'};
    my $params = $nettool->{'parameters'};
    `($path$cmd $options $params; echo "EOF") >/tmp/tmp_$cmd 2>/tmp/tmp_${cmd}_error &`;

    return $true;
}


#==============================================================================
# Stop the nettool background process
#==============================================================================
sub stop
{
    my ($nettool,$how) = @_;

    if ( $how =~ /all/ )
    {
	# Kill the background process
	my $cmd = $nettool->{'cmd'};
	`killall $cmd`;
	`if [ -f /tmp/tmp_$cmd ]; then rm -f /tmp/tmp_$cmd; fi`;
	`if [ -f /tmp/tmp_${cmd}_error ]; then rm -f /tmp/tmp_${cmd}_error; fi`;
    }

    # End of the process state progression
    $nettool->{'process_state'} = 0;

    # Nettool is now ready to start
    $nettool->{'ready'} = $true;

    return $true;
}


#==============================================================================
# Get background process result
#==============================================================================
sub get_background_process_result
{
    my ($nettool) = @_;

    # if the background process is not running then return
    if (not $nettool->running)
    {
	return $false;
    }

    my $cmd_line = $nettool->{'process_line'};
    my $cmd = $nettool->{'cmd'};
    my @record = `tail +${cmd_line} /tmp/tmp_$cmd`;

    return @record;
}


#==============================================================================
# Actualize finger parameters
#==============================================================================
sub actualize
{
    my ($nettool,$inc) = @_;
    $nettool->{'process_line'} += $inc;

    # Calculate process state
    my $pb = $nettool->process_state;
    ($pb == 0) && ($pb = 1) || ($pb = 0);
    $nettool->{'process_state'} = $pb;

    return $true;
}


#=============================================================================
#== Get background process errors and warnings
#=============================================================================
sub get_errors
{
    my ($nettool) = @_;
    my $cmd = $nettool->{'cmd'};
    return `if [ -f /tmp/tmp_finger_error ]; then cat /tmp/tmp_${cmd}_error ; fi`;
}

sub get_warnings
{
    my ($nettool) = @_;
    my $cmd = $nettool->{'cmd'};
    return `if [ -f /tmp/tmp_finger_error ]; then cat /tmp/tmp_${cmd}_error ; fi`;
}


#=============================================================================
#== Set and get some of nettool object variables
#=============================================================================
sub running
{
    my ($nettool) = @_;
    return not $nettool->{'ready'};
}

sub options
{
    my ($nettool) = @_;
    return $nettool->{'options'};
}

sub set_options
{
    my ($nettool, $options) = @_;
    $nettool->{'options'} = $options;
    return $true;
}

sub process_state
{
    my ($nettool) = @_;
    return $nettool->{'process_state'};
}

sub names
{
    my ($nettool) = @_;
    return $nettool->{'names'};
}

sub path
{
    my ($nettool) = @_;
    return $nettool->{'path'};
}

sub set_path
{
    my ($nettool, $path) = @_;
    $nettool->{'path'} = $path;
    return $true;
}

sub cmd
{
    my ($nettool) = @_;
    return $nettool->{'cmd'};
}

sub param
{
    my ($nettool) = @_;
    return $nettool->{'parameters'};
}

1;

__END__
