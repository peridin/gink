# GINK Interface Nettool Kit

Project launched in 2003.

Previous in https://savannah.nongnu.org/projects/gink 

This project is not part of the GNU Project.

My project is a kind of net tool box under GNU/Linux.

It's mainly a user graphic interface to system commands like ping,
nmap, finger, traceroute, tcpdump, netstat, whois, lookup, arp tools,
etc...

The main goal is to facilitate the use of them and mainly to give
easier use of one command from the result of another one.

The graphic interface is in Gnome/Gtk+ for a best integration in a
Gnome environment.

The look and functionalities of this software look like an equivalent
under MAC OS X.

A setting part will give the availibility to use these net tools with a
configuration of options as richer as their equivalent command line.

The result of each command is colorized in some way for better
visibility and interpretation.

The user can right click on a selected part of a tool result to pop up
a context mouse menu. In this context menu he can launch another tool
with the selected part as entry.

In the installation the software will try to find needed commands. The
functionalities of not founded commands will be disabled (except if the
user change settings, for command path for example).

A working preversion exists with some tools like finger, ping,
traceroute, nmap, netstat, tcpdump. Context mouse menu, settings and other
tools are not implemented yet.

- Date d'enregistrement : jeu. 09 janv. 2003 10:57:33 UTC
- Licence : Licence publique générale GNU v2 ou ultérieure
- Stade de développement : 1 - Planification
