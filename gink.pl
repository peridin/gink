#!/usr/bin/perl
#
#    This file is part of Gink.
#
#    Gink is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#==============================================================================
#
# gink.pl
#
# Copyright 2003 Yann Le Thieis
# Contact:  Yann Le Thieis - <lethieis@yahoo.fr>
#
#==============================================================================
#=== This is a toplevel script
#==============================================================================
# $Id: gink.pl,v 1.1.1.1 2003/01/24 08:35:20 peredin Exp $
#==============================================================================

require 5.000; use strict 'vars', 'refs', 'subs';

package app_gink;

BEGIN
{
    use lib "./";
    use src::Gink;
    use vars qw(@ISA);
#    use Carp qw(cluck);
#        $SIG{__DIE__}  = &Carp::confess;
#        $SIG{__WARN__} = &Carp::cluck;
}

$Glade::PerlRun::pixmaps_directory = "/home/yann/Projets/gink/pixmaps";

select STDOUT; $| = 1;

my %params = (
);

__PACKAGE__->app_run(%params) && exit 0;

exit 1;

1;

__END__

